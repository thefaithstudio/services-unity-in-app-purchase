﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;


public class IAPHandler : MonoBehaviour, IStoreListener {

	#region CUSTOM DATATYPE

	[System.Serializable]
	public struct IAPProduct
	{
		public string productID;

		[HideInInspector]
		public string productName;
		[HideInInspector]
		public string productDescription;
		[HideInInspector]
		public string productPrice;
		[HideInInspector]
		public bool	isProductAlreadyPurchased;

		public ProductType productType;

		public UnityEvent OnPurchaseSuccesful;
		public UnityEvent OnPurchaseFailed;
		public UnityEvent OnRestoreSuccesful;
		public UnityEvent OnRestoreFailed;
	}

	#endregion

	#region Variables

	public static 	IAPHandler Instance{ get; set;}

	public bool testModeGUI;
	public bool enableSingleton;

	public UnityEvent	OnTransactionFailed;

	public IAPProduct[] iapProduct;

	//OnEditor
	public bool[] onDisplayProduct;

	//-------------------------------

	private static 	IStoreController 	StoreController;
	private static 	IExtensionProvider	ExtensionProvider; 

	private string 	bundleID;

	//OnGUI
	private int productIDOnGUI;

	#endregion

	//------------------------------

	#region Pre/Post Process

	void Awake(){

		mPreProcess ();
	}

	void OnGUI(){

		if (testModeGUI) {

			float width = 0;
			float height= 0;

			if (Screen.width > Screen.height) {

				width = Screen.width / 3;
				height = Screen.height / 16;
			} else {

				width = Screen.width / 2;
				height = Screen.height / 12;
			}

			GUI.TextField (
				new Rect (0, 0, Screen.width, height),
				IsInitializaed () ? "IAP Initialized" : "IAP Not Initialized");

			//if : Pressed Left Button
			if(GUI.Button(new Rect(0,height,width/4,height),"<-")){

				if (productIDOnGUI > 0) {

					productIDOnGUI--;
				} else
					productIDOnGUI = iapProduct.Length - 1;
			}

			//Showing ProductID
			GUI.TextField (
				new Rect (width/4, height, Screen.width - (width/2), height),
				iapProduct[productIDOnGUI].productID
			);

			//if : Pressed Right Button
			if(GUI.Button(new Rect(Screen.width - (width/4),height,width/4,height),"->")){

				if (productIDOnGUI < (iapProduct.Length - 1)) {

					productIDOnGUI++;
				} else
					productIDOnGUI = 0;
			}

			//Product Description
			GUI.TextField (
				new Rect (0, height*2, Screen.width, height),
				iapProduct[productIDOnGUI].productDescription
			);

			if(GUI.Button(new Rect(0,height*3,Screen.width/2,height),"Buy")){

				BuyProduct (productIDOnGUI);
			}
			if(GUI.Button(new Rect(Screen.width/2,height*3,Screen.width/2,height),"Restore")){

				RestoreProduct (productIDOnGUI);
			}
		}
	}

	private void mPreProcess(){

		//Singleton Or Not
		if (enableSingleton) {

			if (Instance == null) {

				Instance = this;
			} else if(Instance != this){

				Destroy (Instance.gameObject);
				Instance = this;
			}

			DontDestroyOnLoad (gameObject);
		} else {

			Instance = this;
		}

		//-----------------------------

		bundleID = Application.identifier;

		for (int index = 0; index < iapProduct.Length; index++) {

			#if UNITY_IOS
			iapProduct [index].productID = bundleID + "." + iapProduct [index].productID;
			#endif
		}

		if (StoreController == null) {

			InitializePurchasing ();
		}


	}

	private bool IsInitializaed(){

		return StoreController != null && ExtensionProvider != null;
	}

	private void InitializePurchasing(){

		if (!IsInitializaed ()) {

			Debug.Log ("IAP - not initialized");

			var IAPBuilder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance());

			for(int index = 0 ; index < iapProduct.Length ; index++){

				IAPBuilder.AddProduct (iapProduct [index].productID, iapProduct [index].productType);
			}

			UnityPurchasing.Initialize (this, IAPBuilder);

			Debug.Log ("IAP - Initialization complete");
			for(int index = 0 ; index < iapProduct.Length ; index++){

				Product mProduct = StoreController.products.WithID (iapProduct[index].productID);
				iapProduct [index].productName			= mProduct.metadata.localizedTitle;	
				iapProduct [index].productDescription 	= mProduct.metadata.localizedDescription;
				iapProduct [index].productPrice 		= mProduct.metadata.localizedPriceString;

				if (mProduct.hasReceipt)
					iapProduct [index].isProductAlreadyPurchased = true;
				else
					iapProduct [index].isProductAlreadyPurchased = false;
			}
		}else
			Debug.Log ("IAP - already initialized");
	}

	#endregion

	//------------------------------

	#region IStoreListener Product

	public void OnInitialized(IStoreController mStoreController, IExtensionProvider mExtensionProvider){

		StoreController 	= mStoreController;
		ExtensionProvider	= mExtensionProvider;

		Debug.Log ("IAP - initialized");
	}

	public void OnInitializeFailed(InitializationFailureReason error){

		Debug.Log ("IAP - failed to initialization : " + error);
	}


	#endregion

	//------------------------------

	#region Purchase Callback

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args){

		if (String.Equals (args.purchasedProduct.definition.id, "", StringComparison.Ordinal)) {

			Debug.Log(
				string.Format("OnPurchase: SUCCESS - Product: '{0}'", 
					args.purchasedProduct.definition.id));

			for (int index = 0; index < iapProduct.Length; index++) {

				if (iapProduct [index].productID == args.purchasedProduct.definition.storeSpecificId) {

					iapProduct [index].OnPurchaseSuccesful.Invoke ();
					break;
				}
			}
			//args.purchasedProduct.transactionID : The unique id of the transaction
			//PostPurchase Method should be call from here

		}

		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product mProduct, PurchaseFailureReason mPurchaseFailureReason){

		Debug.Log(string.Format(
			"OnPurchase: FAILED. Product: '{0}', PurchaseFailureReason: {1}", 
			mProduct.definition.storeSpecificId, 
			mPurchaseFailureReason));

		for (int index = 0; index < iapProduct.Length; index++) {

			if (iapProduct [index].productID == mProduct.definition.storeSpecificId) {

				iapProduct [index].OnPurchaseFailed.Invoke ();
				break;
			}
		}
	}

	#endregion

	//------------------------------

	#region Purchase/Restore  Callback

	public void BuyProduct(int mProductIndex){

		if (IsInitializaed ()) {

			Product mProduct = StoreController.products.WithID (iapProduct[mProductIndex].productID);

			if (mProduct != null && mProduct.availableToPurchase) {

				StoreController.InitiatePurchase (iapProduct[mProductIndex].productID);
			} else {

				//Needs to be handle by the developer
				Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		} else {

			//Failed most of the cases cause of internet
			Debug.Log("BuyProductID FAIL. Not initialized.");
			OnTransactionFailed.Invoke ();
		}
	}

	public void RestoreProduct(int mProductIndex){

		if (IsInitializaed ()) {

			#if UNITY_IOS

			var appStore = ExtensionProvider.GetExtension<IAppleExtensions> ();

			appStore.RestoreTransactions ((result) => {

			if (result) {

			for (int index = 0; index < iapProduct.Length; index++) {

			if (iapProduct [index].productType == ProductType.NonConsumable) {

			Product mProduct = StoreController.products.WithID (iapProduct [index].productID);

			if (mProduct.hasReceipt) {

			Debug.Log ("Restore Purchase : Succesful -> " + mProduct.receipt);
			iapProduct [index].OnRestoreSuccesful.Invoke ();
			} else {

			Debug.Log ("Restore Purchase : Failed as it is not been purchased yet");
			iapProduct [index].OnRestoreFailed.Invoke ();
			}

			break;
			}else{

			Debug.Log ("Restore Purchase : Failed for '" + iapProduct[index].productType + "', as it is only applicable for non-consumable product");
			}
			}
			} else {

			Debug.Log ("Restore Purchase : Failed for technical difficulty");
			OnTransactionFailed.Invoke ();
			}
			});
			#else
			Debug.Log("Restore Purchase FAIL. Not supported to the current platform : " + Application.platform); 
			#endif
		} else {
			Debug.Log ("IAP - not initialized");
			OnTransactionFailed.Invoke ();
		}
	}

	public string GetProductID(int mProductIndex){
	
		if (mProductIndex >= 0 && mProductIndex < iapProduct.Length) {

			return iapProduct [mProductIndex].productID;
		} else {

			Debug.Log ("Invalid Product Index");
			return "Invalid Product Index";
		}
	}

	public string GetProductName(int mProductIndex){

		if (mProductIndex >= 0 && mProductIndex < iapProduct.Length) {

			return iapProduct [mProductIndex].productName;
		} else {

			Debug.Log ("Invalid Product Index");
			return "Invalid Product Index";
		}
	}

	public string GetProductDescription(int mProductIndex){

		if (mProductIndex >= 0 && mProductIndex < iapProduct.Length) {

			return iapProduct [mProductIndex].productDescription;
		} else {

			Debug.Log ("Invalid Product Index");
			return "Invalid Product Index";
		}
	}
		
	public string GetProductPrice(int mProductIndex){

		if (mProductIndex >= 0 && mProductIndex < iapProduct.Length) {

			return iapProduct [mProductIndex].productPrice;
		} else {

			Debug.Log ("Invalid Product Index");
			return "Invalid Product Index";
		}
	}

	public ProductType GetProductType(int mProductIndex){
	
		if (mProductIndex < 0 && mProductIndex >= iapProduct.Length) {
		
			Debug.Log ("Invalid Product Index");
		} 

		return iapProduct [mProductIndex].productType;
	}

	public bool IsProductAlreadyPurchased(int mProductIndex){

		if (mProductIndex >= 0 && mProductIndex < iapProduct.Length) {

			if (iapProduct [mProductIndex].productType == ProductType.NonConsumable)
				return iapProduct [mProductIndex].isProductAlreadyPurchased;
			else {

				Debug.Log ("Invalid Product Type : " + iapProduct [mProductIndex].productType);
				return false;
			}
		} else {

			Debug.Log ("Invalid Product Index");
			return false;
		}
	}

	#endregion
}

