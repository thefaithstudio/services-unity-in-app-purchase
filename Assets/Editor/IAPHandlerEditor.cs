﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IAPHandler))]
public class IAPHandlerEditor : Editor {

	private IAPHandler mIAPHreference;
	private int numberOfIAPProduct;

	private void mPreProcess(){

		mIAPHreference = (IAPHandler)target;
	}

	private void OnEnabled(){

		mPreProcess ();
	}

	public override void OnInspectorGUI(){

		mPreProcess ();

		serializedObject.Update ();

		EditorGUILayout.Space ();
		for (int index = 0; index < mIAPHreference.iapProduct.Length; index++) {

			#if UNITY_ANDROID

			EditorGUILayout.LabelField (
			"Index (" + index + ") -> " + mIAPHreference.iapProduct [index].productID);

			#elif UNITY_IOS

			EditorGUILayout.LabelField (
				"Index (" + index + ") -> " + Application.identifier + "." + mIAPHreference.iapProduct [index].productID);

			#endif
		
		}

		//-----------------------------------------------------------------------

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.PropertyField(
			serializedObject.FindProperty ("testModeGUI"));
		EditorGUILayout.PropertyField(
			serializedObject.FindProperty ("enableSingleton"));
		EditorGUILayout.EndHorizontal ();

		//-----------------------------------------------------------------------

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));
		EditorGUILayout.HelpBox (
			"Use the following callback to create a dialog box to provide any general fail transaction information" +
			" to user such as technical failure or no internet connection",
			MessageType.Info);
		EditorGUILayout.PropertyField(
			serializedObject.FindProperty ("OnTransactionFailed"));

		//-----------------------------------------------------------------------

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (5.0f));
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Create IAP Products")) {

			if (numberOfIAPProduct != mIAPHreference.iapProduct.Length) {

				if (mIAPHreference.onDisplayProduct == null)
					mIAPHreference.onDisplayProduct = new bool[numberOfIAPProduct];

				int mBoundary = numberOfIAPProduct > mIAPHreference.iapProduct.Length ? mIAPHreference.iapProduct.Length : numberOfIAPProduct; 
				IAPHandler.IAPProduct[] mBackup = new IAPHandler.IAPProduct[mBoundary];
				bool[] mOnDisplayProduct = new bool[mBoundary];
				for (int index = 0; index < mBoundary; index++) {

					mBackup [index] = mIAPHreference.iapProduct [index];
					mOnDisplayProduct [index] = mIAPHreference.onDisplayProduct [index];
				}

				mIAPHreference.iapProduct = new IAPHandler.IAPProduct[numberOfIAPProduct];
				mIAPHreference.onDisplayProduct = new bool[numberOfIAPProduct];
				for (int index = 0; index < mBoundary; index++) {

					mIAPHreference.iapProduct [index] = mBackup [index];
					mIAPHreference.onDisplayProduct[index] = mOnDisplayProduct [index];
				}
			}
		}
		numberOfIAPProduct = EditorGUILayout.IntField (
			numberOfIAPProduct);
		EditorGUILayout.EndHorizontal ();

		EditorGUI.indentLevel += 1;
		for (int index = 0; index < mIAPHreference.iapProduct.Length; index++) {

			mIAPHreference.onDisplayProduct [index] = EditorGUILayout.Foldout
				(mIAPHreference.onDisplayProduct [index],
					(mIAPHreference.iapProduct [index].productID !=null && mIAPHreference.iapProduct [index].productID.Length != 0) ?
					mIAPHreference.iapProduct [index].productID : "Element (" + index.ToString () + ")");

			if (mIAPHreference.onDisplayProduct [index]) {

				EditorGUI.indentLevel += 1;

				mIAPHreference.iapProduct [index].productID = EditorGUILayout.TextField (
					"Product ID",
					mIAPHreference.iapProduct [index].productID);

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("iapProduct").
					GetArrayElementAtIndex (index).
					FindPropertyRelative ("productType"));

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("iapProduct").
					GetArrayElementAtIndex (index).
					FindPropertyRelative ("OnPurchaseSuccesful"));

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("iapProduct").
					GetArrayElementAtIndex (index).
					FindPropertyRelative ("OnPurchaseFailed"));

				#if UNITY_IOS

				EditorGUILayout.PropertyField (
				serializedObject.FindProperty ("iapProduct").
				GetArrayElementAtIndex (index).
				FindPropertyRelative ("OnRestoreSuccesful"));

				EditorGUILayout.PropertyField (
				serializedObject.FindProperty ("iapProduct").
				GetArrayElementAtIndex (index).
				FindPropertyRelative ("OnRestoreFailed"));

				#endif

				EditorGUI.indentLevel -= 1;
			}
		}
		EditorGUI.indentLevel -= 1;

		serializedObject.ApplyModifiedProperties ();
	}
}
