# Documentation
You can find the full **Documentation** on the following [link](https://bitbucket.org/PGS_ART/service-unity-in-app-purchase/src/075ec56f84104c14e8daecdbe4c369ab7ee98367/Assets/HelpTool/Documentation.docx?at=master&fileviewer=file-view-default)

# Editor Settings

- **Index (X):** It's show the "productID" of the following configured product. The "X" denotes the of the product you wish to work with.

- **Test Mode GUI:** Allow you to test the unity in app purchase services with GUI buttons to test on. Physical device with callback

- **Enable Singleton:** By checking the box, there will be one game object of "IAPHandler" script on entire game life cycle. (Checking the box is recommended as most of the transaction happen from the one scene view like shop scene).

- **OnTransactionFailed:** Use the following callback to create a dialog box to provide any general fail transaction information to user such as technical failure or no internet connection.

- **Create IAP Products:** To create the **IAP Product** , type the number of product your game has and click on the "Create IAP Product". After creating the product, fill the **"ProductID"** input field with your product ID which you assign to your playstore/appstore. The difference between the playstore and appstore product ID is one include bundle id (Appstore: com.pechasgamestudio.cricketclash.PremiumUser) and other don't (PlayStore: PremiumUser). The issue between the platform are handled by the script, so just simply fill with the unique product ID (PremiumUser) for both platform, the script will handle the **"bundle_Id"** issue to itself.

- **OnPurchaseSuccesful:** Use the following callback when the product is successfully purchased.

- **OnPurchaseFailed:** Use the following callback when the product is failed to purchased.

- **OnRestoreSuccesful:** Use the following callback when the product is successfully restored. The following feature only gets available when you changed the build setting from "File -> Build Settings -> iOS -> Switch Platform".

- **OnRestoreFailed:** Use the following callback when the product is failed to restored. The following feature only gets available when you changed the build setting from File -> Build Settings -> iOS -> Switch Platform".

# Custom Function

Overview: If you keep the function singleton, that it's guaranteed that all the information of the IAP can be simply get by calling **"IAPHandler.Instance.YourFunction"**. If it's not singleton, then make sure you call by the reference.

- **BuyProduct(int mProductIndex)**: Pass the product index to buy the specific product.
- **RestoreProduct(int mProductIndex)**: Pass the product index to restore the specific product.
- **GetProductID(int mProductIndex)**: return "ProductID" if you provide "Product Index".
- **GetProductName(int mProductIndex)**: return "ProductName" if you provide "Product Index".
- **GetProductDescription(int mProductIndex)**: return "ProductDescription" by providing "Product Index".
- **GetProductPrice(int mProductIndex)**: return "ProductPrice" by providing "Product Index".
- **GetProductType(int mProductIndex)**: return "Product Type (Consumable/Non-Consumable/Subscription)" by providing "Product Index".
- **IsProductAlreadyPurchased(int mProductIndex):** return  true, if the product is purchased(non-consumable)

